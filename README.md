# Custom Developer Console
- [x] Webpage Direct Editor (Pen)
- [x] Element Inspector (Magnifying Glass)
- [x] Partially Finished Main Console
- [x] Partially Finished HTML Inspector
- [x] Almost Done With Sources Viewer

# Notes
These custom Developer Tools isn't perfect, some secure sites disable inline-scripts, which breaks the Developer Tools.

**Basic Instructions with Console**
1. Open the Developer Console.
   1. Methods Include: Pressing *f12*
   1. Pressing *Ctrl + Shift + i*
   1. Right Clicking then Pressing '*Inspect*'
1. Copy the Script from *main.js*.
1. Paste the Script in the Developer Console.
1. Press *Enter* to Run the Script.

**Basic Instructions without Console**

Install the bookmarklet

[Install the bookmarklet here by pressing this link. Then, on that page drag the link to your bookmarks bar. If you do not see a bookmarks bar then press Ctrl-Shift-B.](https://era2010-coder.github.io/devtools/)

Using the bookmarklet

To use the bookmarklet, you have to click the bookmarklet in your bookmarks bar.
